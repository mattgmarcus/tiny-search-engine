/*

  FILE: file.c

  Description: This file contains the dirent structure that is used for accessing the filenames in the target directory. When indexer.c needs the next filename, it will call FindFilesInPath to get that filename. The dirent structure, the current index, and the number of files are stored as a static members so that we can keep track of where in namelist we are.
  This also contains the readFile method
*/


#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include "header.h"
#include <dirent.h>
#include "../indexer/indexer.h"
#include "indexerutils.h"
#include "file.h"
#include <string.h>

//Static structures used when accessing namelist
static struct dirent **namelist = NULL;
static int currentfilenum = 0;
static int numfiles;

/*
  Method to return a filename in the path we're given. Creates namelist upon the first call
  @param path - target directory path
  @return name of file
*/
char *FindFilesInPath(char *path) {
  //Create namelist on the first call
  if ( NULL == namelist ) {
    numfiles = scandir(path, &namelist, 0, versionsort);
  }

  //If there are only 2 files, then that means there are really none since those two correspond to . and ..
  if ( 2 == numfiles ) {
    return NULL;
  }

  //While we haven't one through all of namelist, return the next file
  if ( currentfilenum < numfiles ) {
    return namelist[currentfilenum++]->d_name;
  }

  //If we've already returned all the files in namelist, return NULL
  else {
    return NULL;
  }
}

/*
  Method to cleanup the namelist structure. Called from cleanup() in indexer.c
*/
void dircleanup() {
  while (numfiles--) {
    free(namelist[numfiles]);
  }
  free(namelist);
}


/*
  Method to create a new indexer structure when given a file that has the information outputted by a prior indexer
  @param dir - the directory where the file is in
  @param file - the name of the file with the information
  @return indexer2 - an inverted index with the information
 */
INVERTED_INDEX *readFile(char *dir, char *file) {
  INVERTED_INDEX *indexer = (INVERTED_INDEX *)malloc( sizeof(INVERTED_INDEX) );
  MALLOC_CHECK(indexer);
  BZERO(indexer, sizeof(INVERTED_INDEX));

  //Initialize both the start and end of indexer to be null
  indexer->start = indexer->end = NULL;

  //The pathname has the location of the file to read
  char *pathname = malloc(strlen(dir) + strlen(file) + 5);
  MALLOC_CHECK(pathname);
  BZERO(pathname, (strlen(dir) + strlen(file) + 5));
  strcat(pathname, dir);
  strcat(pathname, file);

  //Open the file and make sure it's not NULL. If it is, return NULL
  FILE *fp = fopen(pathname, "r");

  if ( NULL == fp ) {
    return NULL;
  }

  //word will store the words we read in from the .dat file
  char word[WORD_LENGTH];
  BZERO(word, WORD_LENGTH);

  int numdocs, docID, numtimes;

  //While the next word in resultsfile2 isn't null, get the information associated with that word to construct the inverted index
  fscanf(fp, "%s", word);
  while ( 0 != strcmp(word, "\0") ) {
    fscanf(fp, "%d", &numdocs);

    while ( numdocs-- ) {
      fscanf(fp, "%d %d", &docID, &numtimes);

      while ( numtimes-- ) {
	updateIndex(word, docID, indexer);
      }
    }

    BZERO(word, WORD_LENGTH); //Reset word
    fscanf(fp, "%s", word);
  }

  free(pathname);
  fclose(fp);

  return indexer;
}
