/*

  FILE: input.c

  Description: This file contains some utilities that are helpful when checking the user's input
  These functions are:
  2 that check the number of inputs from the user. One of them is the case where the user is expected to have only one number of inputs, the other is when they could have one of two numbers of inputs
  testdir - makes sure that a directory is valid
  testfile - makes sure that a filename is valid
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "input.h"

/*
  Method to check the amount of inputs the user gave. If they didn't give the number required, then print an error to stderr and exit on error
  @param numargs - the number of arguments the user gave
  @param numreq - the number of arguments required by the program
 */
void checknuminputsone(int numargs, int numreq){
    if ( numreq != numargs ) {
    fprintf(stderr, "--------------------------------------------------\n");
    fprintf(stderr, "Invalid number of inputs. You entered %d. Enter %d\n", numargs, numreq);
    fprintf(stderr, "--------------------------------------------------\n");
    exit(1);
  }
}


/*
  Method to check the amount of inputs the user gave. If they didn't give the number required, then print an error to stderr and exit on error. This version of function lets the user input one of two numbers of inputs
  @param numargs - the number of arguments the user gave
  @param numreq1 - the first option for number of arguments required by the program
  @param numreq2 - the second option for number of arguments required by the program
 */
void checknuminputstwo(int numargs, int numreq1, int numreq2) {
  if ( ( numreq1 != numargs ) && ( numreq2 != numargs ) ) {
    fprintf(stderr, "--------------------------------------------------------\n");
    fprintf(stderr, "Invalid number of inputs. You entered %d. Enter %d or %d\n", numargs, numreq1, numreq2);
    fprintf(stderr, "--------------------------------------------------------\n");
    exit(1);
  }
}

/*
  Method to test the directory that a user inputs. It makes sure that it exists, is a directory, and is writable. If any of these conditions aren't met, the program prints an error to stderr and exits on error
  @param dir - the name of the directory to be checked
 */
void testDir(char *dir){
  struct stat statbuf;
  if ( stat(dir, &statbuf) == -1 ) {
    fprintf(stderr, "-----------------\n");
    fprintf(stderr, "Invalid directory\n");
    fprintf(stderr, "-----------------\n");
    exit(1);
  }

  //Both check if there's a directory and if it's writable
  if ( !S_ISDIR(statbuf.st_mode) ) {
    fprintf(stderr, "-----------------------------------------------------\n");
    fprintf(stderr, "Invalid directory entry. Your input isn't a directory\n");
    fprintf(stderr, "-----------------------------------------------------\n");
    exit(1);
  }
  if ( (statbuf.st_mode & S_IWUSR) != S_IWUSR ) {
    fprintf(stderr, "------------------------------------------\n");
    fprintf(stderr, "Invalid directory entry. It isn't writable\n");
    fprintf(stderr, "------------------------------------------\n");
    exit(1);
  }
}

/*
  Method to test a file to see if it exists. If it does, then the function returns 1, otherwise 0. The function that calls testfile can decide whether a return value of 1 or 0 is good
  @param filename - the name of the file to check
  @return 1 on file's existence, 0 otherwise
 */
int testfile(char *filename) {
  FILE *fp = fopen(filename, "r");

  if ( NULL == fp ) {
    return 0;
  }

  else {
    fclose(fp);
    return 1;
  }
}
