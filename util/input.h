#ifndef _INPUT_H_
#define _INPUT_H_

//These methods will be used when checking the inputs the user gives
void checknuminputsone(int numargs, int numreq);

void checknuminputstwo(int numargs, int numreq1, int numreq2);

void testDir(char *dir);

int testfile(char *filename);

#endif
