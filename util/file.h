#ifndef _FILE_H_
#define _FILE_H_

#include "../indexer/indexer.h"

//These are the two methods that are in file.c
char *FindFilesInPath(char *path);
void dircleanup();
INVERTED_INDEX *readFile(char *dir, char *file);

#endif
