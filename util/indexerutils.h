#ifndef _INDEXERUTILS_H
#define _INDEXERUTILS_H

//These are the functions that are defined further in indexerutils.c

void cleanupindexer(INVERTED_INDEX *myindex);

void iaddexisting(char *word, int documentID, WORDNODE *current_wnode);

int updateIndex(char *word, int documentID, INVERTED_INDEX *myindex);

WORDNODE *iaddnew(char *word, int documentID);

int saveIndexToFile(char *dir, char *filename, INVERTED_INDEX *myindex);

#endif
