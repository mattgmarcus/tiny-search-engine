/*

  FILE: indexerutils.c

  Description: This file contains some of the utilities that I found helpful for the indexer function. I also thought it was useful to separate this from indexer to allow my query.c file to access these methods more easily.
*/
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include "hash.h"
#include "../indexer/indexer.h"
#include "indexerutils.h"
#include "header.h"

/*
  Method to clean up the indexer2 data structure
  @param myindex - indexer2 will be the index passed in
*/
void cleanupindexer(INVERTED_INDEX *myindex) {
  //Cleanup the indexer2 structure
  WORDNODE *current_wnode = myindex->start;
  while ( current_wnode != myindex->end ) {
    current_wnode = current_wnode->next;
    DOCUMENTNODE *current_dnode = current_wnode->prev->page;
    DOCUMENTNODE *temp_dnode;
    while ( NULL != current_dnode->next ) {
      temp_dnode = current_dnode->next;
      free(current_dnode);
      current_dnode = temp_dnode;
    }
    free(current_dnode);
    free(current_wnode->prev);
  }
  //That loop won't free the last element in indexer2, so do that here
  free(myindex->end->page);
  free(myindex->end);

  //Free indexer2 itself
  free(myindex);
}

/*
  Method to update the index given a word, it's document ID, and the index
  @param word - word to be added in
  @param documentID - the ID of the document the word was in
  @param myindex - index for the word to be added into
  @return 1 upon succcess
*/
int updateIndex(char *word, int documentID, INVERTED_INDEX *myindex) {
  long hashnum = ( hash1(word) % MAX_HASH_SLOT );
  int appendword = 0; //let's us know if we already have the word in myindex

  //If the index doesn't have the word already, add it in
  if ( NULL == myindex->hash[hashnum] ) {
    WORDNODE *wnode = iaddnew(word, documentID);
    wnode->numdocs = 1;

    myindex->hash[hashnum] = wnode;

    //If this is the very first word that's being added into myindex, then initialize start and end to point to this wnode
    if ( ( NULL == myindex->start ) && ( NULL == myindex->end ) ) {
      myindex->start = wnode;
      myindex->end = wnode;
      myindex->start->prev = wnode;
      myindex->end->prev = wnode;
      myindex->start->next = wnode;
      myindex->end->next = wnode;
    }

    //Otherwise, just sequence the wnode into the index structure
    else {
      myindex->end->next = wnode;
      wnode->prev = myindex->end;
      myindex->end = wnode;
      wnode->next = myindex->start;
      myindex->start->prev = wnode;
    }
    return 1;
  }

  //Go here if the hashslot is already filled
  else {
    //Check if this word is the same one as the first word hashed in the index
    //If it's not the same word, go here
    if ( 0 != strcmp((myindex->hash[hashnum]->word), word) ) {
      WORDNODE *current_wnode = myindex->hash[hashnum]->next;

      //While we're at WORDNODE's that have this hash number, keep looking to see if the word is there. If we do find it, set appendword to true (1) since we just have to add in a new spot where we found it.
      //If we eventually get past the entries with this hash number, then the word isn't there, so add it in
      while ( ( myindex->start != myindex->end ) && ( myindex->start != current_wnode ) && ( hashnum == (hash1(current_wnode->word)%MAX_HASH_SLOT) ) ) {
	if ( 0 == strcmp(current_wnode->word, word) ) {
	  appendword = 1;
	  break;
	}
	current_wnode = current_wnode->next;
      }

      //If there is already a wnode for this word, just add in another DOCUMENTNODE for the word
      if ( appendword ) {
	iaddexisting(word, documentID, current_wnode);

	appendword = 0; //reset appendword for the next run
      }
      
      //If there is no wnode for the word, create one and put it into myindex
      else {
	WORDNODE *wnode = iaddnew(word, documentID);
	wnode->numdocs = 1;

	//Go back to the last item in this spot of the hash table
	current_wnode = current_wnode->prev;

	current_wnode->next->prev = wnode;
	wnode->next = current_wnode->next;
	current_wnode->next = wnode;
	wnode->prev = current_wnode;

	if ( current_wnode == myindex->end ) {
	  myindex->end = wnode;
	}
      }
      
      return 1;
    }
    //If the current hashslot has this word, go here
    else {
      //Since there's already a wnode for this word, just add in another DOCUMENTNODE for the word
      iaddexisting(word, documentID, myindex->hash[hashnum]);
      
      return 1;
    }
  }
}

/*
  Method to update a WORDNODE structure's linked list of DOCUMENTNODE's to include a new word and the document it was found in
  @param word - word to be added
  @param documentID - doc ID the word was found in
  @param current_wnode - the WORDNODE that has the word already
*/
void iaddexisting(char *word, int documentID, WORDNODE *current_wnode) {
  DOCUMENTNODE *current_dnode = current_wnode->page;

  //Loop through all the document nodes until we either reach the end, in which case this document isn't there yet, or we reach the dnode for this docID
  while ( ( NULL != current_dnode->next ) && ( documentID != current_dnode->document_id ) ) {
    current_dnode = current_dnode->next;
  }

  //If there's already a DOCUMENTNODE for this word, just increase the frequency count
  if ( documentID == current_dnode->document_id ) {
    current_dnode->page_word_frequency += 1;
  }

  //Otherwise, make a new DOCUMENTNODE for this word and add it into the linked list
  else {
    DOCUMENTNODE *dnode = (DOCUMENTNODE *)malloc( sizeof(DOCUMENTNODE) );
    MALLOC_CHECK(dnode);
    BZERO(dnode, sizeof(DOCUMENTNODE));
    dnode->document_id = documentID;
    dnode->page_word_frequency = 1;
    dnode->next = NULL;

    current_dnode->next = dnode;

    current_wnode->numdocs += 1;
  }
}

/*
  Method to create a new WORDNODE for a word in the index structure
  @param word - word to be added
  @param documentID - doc ID of the word
  @return wnode - the new WORDNODE
*/
WORDNODE *iaddnew(char *word, int documentID) {
  //Make a new DOCUMENTNODE and WORDNODE for the word, storing the dnode in the wnode
  DOCUMENTNODE *dnode = (DOCUMENTNODE *)malloc( sizeof(DOCUMENTNODE) );
  MALLOC_CHECK(dnode);
  BZERO(dnode, sizeof(DOCUMENTNODE));
  dnode->document_id = documentID;
  dnode->page_word_frequency = 1;
  dnode->next = NULL;

  WORDNODE *wnode = (WORDNODE *)malloc( sizeof(WORDNODE) );
  MALLOC_CHECK(wnode);
  BZERO(wnode, sizeof(WORDNODE));
  strcpy(wnode->word, word);
  wnode->page = dnode;

  return wnode;
}

/*
  Method to take the index structure and save it into a file
  @param dir - the target directory
  @param filename - name of the file to store it in
  @param myindex - the index to store in the file
  @return 1 upon success, 0 upon error
*/
int saveIndexToFile(char *dir, char *filename, INVERTED_INDEX *myindex) {
  WORDNODE *current_wnode = myindex->start;

  //pathname has the address of the file to write to
  char *pathname = malloc(strlen(dir) + strlen(filename) + 5);
  MALLOC_CHECK(pathname);
  BZERO(pathname, (strlen(dir) + strlen(filename) + 5));
  strcat(pathname, dir);
  strcat(pathname, filename);

  //Open the file. If it's NULL, return 0
  FILE *fp = fopen(pathname, "w");
  if ( NULL == fp ) {
    return 0;
  }

  //Loop until the ned of index, printing the information into the file
  while ( myindex->end != current_wnode ) {
    fprintf(fp, "%s %d ", current_wnode->word, current_wnode->numdocs);

    //Print all the internal dnodes in each wnode
    DOCUMENTNODE *current_dnode = current_wnode->page;

    while ( current_dnode ) {
      fprintf(fp, "%d %d ", current_dnode->document_id, current_dnode->page_word_frequency);
      current_dnode = current_dnode->next;
    }
    
    current_wnode = current_wnode->next;

    fprintf(fp, "\n");
  }

  //Need to do the last element in myindex down here, since that loop above won't capture it
  fprintf(fp, "%s %d ", current_wnode->word, current_wnode->numdocs);
  DOCUMENTNODE *current_dnode = current_wnode->page;
  while ( current_dnode ) {
    fprintf(fp, "%d %d", current_dnode->document_id, current_dnode->page_word_frequency);
    current_dnode = current_dnode->next;
  }
  
  //Free anything we've made here
  fclose(fp);
  free(pathname);

  return 1;
}
