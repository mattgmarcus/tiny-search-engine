README for Tiny Search Engine
=============================

This was the code for a tiny search engine I made in my CS50 class at Dartmouth. This is an application that is run from 
the command line. It implements a search engine of all the websites under the dartmouth.edu domain.


### Setup

With a clean repository, the BATS_TSE.sh script can be used to start up the search engine. After it crawls and indexes
the results, it'll start the query application automatically, allowing you to search the results.

Alternatively, you can issue a 'make clean' command followed by 'make' in each of the util, crawler, indexer, and 
query directories. The 'query' executable can then be run.

There are additional details in the subdirectories as well.


## Parts of original assignment

Below is text from the original readme that was submitted as part of the assignment. It explains some specific 
implementation details that would've been important to the graders


### Top level stuff

The names of my directories/files are slightly different than in the assignment description. My indexer is in the directory indexer, not index. My queryengine is in the query directory. Also, my query file is named query.c instead of queryengine.c. This was just a decision I made based off what seemed better for names.

I have a util directory that contains the utilities needed to run the Tiny Search Engine in a library.

In order to compile the entire system, you can call BATS_TSE.sh, which will go through each directory and call all the respective make files. If you don't want to do that, you can also cd into the following directories: util, crawler, indexer, query
And then call the following commands:

	make clean
	make
	
This will generate all the files needed for the Tiny Search Engine


### BATS_TSE.sh

I should note here that the way my BATS_TSE.sh file works, it follows a slightly different order than the lab description's. I think this makes more sense though. First, starting with crawler, my crawler_test.sh script essentially is my way of running crawler. This will fill up two directories, data2 and data3, with the documents from depths 2 and 3, respectively. Before it does this, it does test the functionality of crawler, but it outputs that into a crawler_testlog file.
The same is true for indexer. When I run the BATS.sh file in the indexer directory, it creates the index.dat files in the data2 subdirectory of crawler.
And in order to test my query engine, I need to be able to access an index.dat file, since the functionality of some methods in query.c rely on the ability to read from an index. So the order I followed in BATS.sh was:

	make library
	make and then test/run crawler
	make and then test/run indexer
	make and then test query
	run query

So once all the tests are completed, query is run

Also, since there was no specification in the lab description, the output from BATS_TSE.sh is just going to stdout. The output here is just the comments from the Makefiles, as well as the output from the queryengine_test executable.

Sidenode about Makefiles: when I run them, I get notices about the timestamps being off for certain files. According to Professor Palmer, that is just a problem on certain machines and it's not a huge deal. I've been working on the Kinsman machine.


### Extra credit for testing

I tested by crawler at hash table sizes 10 and 10000 (as simple as changing the size in dictionary.h). My program ran without a segfault in both cases. When run with a size of 10, it took approximately 20 minutes, returning approximately 840 files, and with a size of 10000, it took approximately 28 minutes, returning 1332 files. This seems like an interesting result, since a smaller hash table took less time, but is probably less accurate. But it didn't crash in either case, so that's good!
