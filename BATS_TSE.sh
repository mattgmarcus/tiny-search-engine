#!/bin/bash
# Script name: BATS_TSE.sh
#
# Description: A bash script to test all the components(crawler, indexer, query)
# of the tiny search engine(TSE)
#
# Input: None
#
# Output: A testlog in both the crawler and indexer directories. Text to stdout
# with the results of the make's and query tests
# This will conclude with query being running
cd util
make clean
make

cd ../crawler
make clean
make
mkdir ./data
mkdir ./data2
mkdir ./data3
mkdir ./nowritedir
chmod 000 ./nowritedir
./crawler_test.sh

cd ../indexer
make clean
make
mkdir "./test"
mkdir ./test2
mkdir ./test2/interior
mkdir ./nowritedir
chmod 000 ./nowritedir
./BATS.sh

cd ../query
make clean
make
make test
./queryengine_test

./query ../crawler/data2/index.dat ../crawler/data2/
