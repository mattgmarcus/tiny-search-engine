README for crawler lab assignment
Matthew Marcus

Design choices:
I made my program quiet to the user except to let them know when things go amiss. Instances where this occurs: 
  One of their inputs is bad
  The crawler tries to crawl a page that isn't valid. If this occurs during the run, then this message goes to stderr but the program doesn't stop
The program also prints when there is nothing left for it to do, so the user knows the program is done.

With the inputs, I chose to have a specific format they must enter the path in. The format must have a slash at the end. For example, this is a valid path: ./data/

For the URLs that are valid, I changed the URL_PREFIX in crawler.c to be just "cs.dartmouth.edu". My program doesn't consider cs.dartmouth.edu, www.cs.dartmouth.edu, and http://cs.dartmouth.edu to be the same. So that page can get crawled more than once. It only happens a few times, though, and doesn't affect the overall implementation of the program.

I also changed the structure of url_list in crawler.c to be char **, instead of char *[]. This made more sense to me, given the way I wanted functions to return values and url_list to store the char *'s.
I also changed the parameters of updateListLinkToBeVisited to not include url_list, since it was a global variable and I saw little point in passing it as a parameter too.

In terms of where the output from my program goes, the output files go into the path. My program doesn't clear the directory it's pointing to, so any old files from previous runs can still be there. That doesn't affect the running of my program, though, since it'll just overwrite them. However, if you run the crawler with a deep depth, then you could end up with a few hundred files in the directory (all following the naming convention of increasing numbers).

For my dict structure, I chose to store it in a doubly linked list, with the start and end DNODES pointing to one another. 

With hash numbers within dict's hash table, I used the hash1 function in hash.c to calculate the hash number. However, sometimes that number is greater than MAX_HASH_SLOT in crawler.h, so I would mod the hash number returned by MAX_HASH_SLOT

In my makefile, the implementations that are available are:
   make clean
   make test
   make
The standard make command is what should be used to get my crawler function

When using my Makefile, a warning will be issued because of teh compilation of header.h. I believe this occurs since the file has the posssibility of not doing anything if header.h is already defined, which the pedantic option in the compiler flags. However, this doesn't affect my implementation.

For webpages that wget cannot download, such as pages with 404 errors, I handled this in two ways. First, I enabled the wget switches that allow wget to timeout if the attempt to download takes too long, as well as the switch that only allows for a certain number of downloads. If a try takes more than ten seconds, it'll abort and go to a second try. If the second try doesn't work, then wget will stop trying to download the page. The system call will return a value other than 1, and then the crawler will report to stderr that there was an issue downloading the url.

For my testing, I have 4 directories that are used. data is used for the tests that don't work, except for the test on a directory that isn't writable. That directory is nowritedir. Unfortunately, the tar won't let me include that since it isn't accessible, so you'll have to create it when you test. If you don't, then my crawler will just indicate that it's invalid. data2 is for storing files from crawling at depth 2. data3 is for depth 3.
