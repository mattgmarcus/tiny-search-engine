#!/bin/bash
# Script name: crawler_test.sh
#
# Description: A script to run tests of the crawler program for Lab 4
#
# Input: none
#
# Output: a file called crawler_testlog.`date`, with the bash output from the date all there
#

date=`date`
filename="crawler_testlog.$date"

touch "$filename"


#First do the tests that show its ability to capture errors
printf "Tests to demonstrate the ability of crawler to capture errors\n" &> "$filename"
printf "Assumptions of this test file are that directories called data, data2, and data3 are accessible with the ./data/ ./data2/ ./data3/ pathes, respectively, and that an accessible directory called ./nowritedir/ is non-writable. The data directory is used for the tests that don't work, data 2 is used for crawler at depth 2, and data 3 for crawler at depth 3\n" &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler\n" &>> "$filename"
printf "Expected output informs the user that they had an inproper number of inputs and exits the program\n" &>> "$filename"
./crawler &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./data/ 5\n" &>> "$filename"
printf "Expected output informs the user of an invalid depth and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./data/ 5 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./data/ abc\n" &>> "$filename"
printf "Expected output informs the user of an invalid depth, since they gave a string and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./data/ abc &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./data/ -1\n" &>> "$filename"
printf "Expected output informs the user of an invalid depth and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./data/ -1 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./fakedir/ 1\n" &>> "$filename"
printf "Expected output informs the user of an invalid directory (assuming the user doesn't have a directory fakedir) that doesn't exist and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./fakedir/ 1 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./nowritedir/ 1\n" &>> "$filename"
printf "Expected output informs the user of an unwritable directory and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./nowritedir/ 1 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu/fakestuffhere ./data/ 1\n" &>> "$filename"
printf "Expected output informs the user of an invalid url and exits the program.\n" &>> "$filename"
./crawler cs.dartmouth.edu/fakestuffhere ./data/ 1 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler google.com ./data/ 1\n" &>> "$filename"
printf "Expected output informs the user of an invalid url that isn't of a cs.dartmouth.edu domain form and exits the program\n." &>> "$filename"
./crawler google.com ./data/ 1 &>> "$filename"


printf "\n\nNow I'll run tests that are expected to work\n" &>> "$filename"
printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./data2/ 2\n" &>> "$filename"
printf "Expected output will be messages when the crawler reaches pages it can't get information from, as well as a notification that the program is finished running. It will create around 198-222 files (the range has varied) in the destination directory as well.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./data2/ 2 &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./crawler cs.dartmouth.edu ./data3/ 3\n" &>> "$filename"
printf "Expected output will be messages when the crawler reaches pages it can't get information from, as well as a notification that the program is finished running. It will create around 1282-1360 files in the destination directory as well.\n" &>> "$filename"
./crawler cs.dartmouth.edu ./data3/ 3 &>> "$filename"
