/*
  File: queryengine_test.c

  Description: Contains tests that make sure that the query engine works properly
  The methods that are used in query.c were copied directly into this file, which worked better than relying on a query.o executable, since then the
  compiler got confused by the two main methods in each file.
  Some code and text from class file: dictionary_test.c. I modeled the format,
  including having functions before main, on this file.
*/

// Test Harness Spec:
// -----------------
//
// It uses this function but it isn not unit tested in this test harness:
//
//      int initLists(char *indexfile);
//
// It does not test this function since the only result of it is printing to 
// stdout, which isn't useful in this scenario, as there's no return value
//
//      void showqueryresults(int *rankeddocs, char *dir);
//
// It tests the following functions:
//
//      char *geturl(char *directory, int docnum);
//      void getwordlist(char *input, char **wordlist);
//      int *rankdocs(int *docnums, int numdocs);
//      void consolidatedocnums(int *docnums, int *finaldocnums, int numdocs);
//      void getdocnumsnew(char *word, int *docnums, int numdocs);
//      void getdocnumsappend(char *word, int *docnums, int numdocs);
//      void cleanupwordlist(char **wordlist);
//
// Test Cases:
// --------------------
// TestGetUrl()
// This test makes sure that the method, geturl, properly reads a url from a
// directory given the file number to look in. I check this on the seed url,
// which in the case of my tests, was just cs.dartmouth.edu
//
// TestGetAndCleanWordlist()
// This test first sees if the wordlist gets initialized properly when the 
// word getwordlist is given is "Welcome". It checks if the first entry in 
// wordlist is "Welcome". Then the structure is cleaned up and we make sure that
// there is now no element in the first spot on wordlist
//
// TestGetDocnumsNew()
// This tests the getdocnumsnew method, which is called when initializing docnums
// for the first time. It gets the document numbers given an input that is just 
// the word "Welcome". Then, it makes sure that the first entry in docnums isn't 0
//
// TestGetDocnumsAppend()
// This tests the getdocnumsappend method. It makes docnums first using the getdocnumsnew 
// function, for the word "Welcome". Then, the getdocnumsappend method is called
// using the word "dartmouth". It checks to make sure that the first entry in docnums
// isn't the default zero, which it shouldn't be since the homepage includes both
//the words "Welcome" and "dartmouth"
//
// TestConsolidationDocNums()
// This tests the ability of the function consolidatedocnums, which given two docnums
// arrays, will perform the OR operation on the arrays. This test calls TestConsolidation,
// which given one of the arrays from the OR operation and the final result of that,
// makes sure that whenever the entry in the first array isn't zero, the corresponding
// entry in the final array also isn't zero, since that would violate the needed
//results of the OR operation
//
// TestRankdocs()
// This method tests the function rankdocs, which given a docnums array, will make
// a new integer array that, as the index of the array increases, the rank of the
// document it stores at that index decreases. This method calls the TestRankeddoc function, 
// which makes sure that the ranking of the document is correct in all cases
// by looking back at the docnums array, where the index was the document number
// and the value was the rank.

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <strings.h>
#include "header.h"
#include "../indexer/indexer.h"
#include "../util/indexerutils.h"
#include "../util/file.h"

//Macros
#define START_TEST_CASE int rs=0

#define SHOULD_BE(x) if (!(x))  {rs=rs+1; \
  printf("Line %d test [%s] Failed\n", __LINE__, #x); \
  }

#define END_TEST_CASE return rs

#define RUN_TEST(x, y) if (!x()) {              \
    printf("Test %s passed\n", y);              \
} else {                                        \
    printf("Test %s failed\n", y);              \
    cnt = cnt + 1;                              \
}

//Global variable
INVERTED_INDEX *indexer;
int wordlistsize = 100;

//Define function prototypes here
char *geturl(char *directory, int docnum);
void getwordlist(char *input, char **wordlist);
int *rankdocs(int *docnums, int numdocs);
void consolidatedocnums(int *docnums, int *finaldocnums, int numdocs);
void getdocnumsnew(char *word, int *docnums, int numdocs);
void getdocnumsappend(char *word, int *docnums, int numdocs);
void cleanupwordlist(char **wordlist);
int initLists(char *indexfile);

int TestGetUrl() {
  START_TEST_CASE;
  //char dir[50] = "../crawler/data2/";
  int docnum = 1;
  char *url = geturl("../crawler/data2/", docnum);
  SHOULD_BE(0 == strcmp(url, "cs.dartmouth.edu\n")); //There's a trailing newline in the file
  free(url);
  END_TEST_CASE;
}

int TestGetAndCleanWordlist() {
  START_TEST_CASE;
  char *input = (char *)malloc(10);
  MALLOC_CHECK(input);
  BZERO(input, 10);
  strcat(input, "Welcome");

  char **wordlist = NULL;
  wordlist = (char **)malloc(sizeof(char *) * (wordlistsize+1));
  MALLOC_CHECK(wordlist);
  BZERO(wordlist, (sizeof(char *) * (wordlistsize+1)));

  getwordlist(input, wordlist);
  SHOULD_BE(0 == strcmp(wordlist[0], input));
  cleanupwordlist(wordlist);
  SHOULD_BE( !wordlist[0] );

  free(input);

  END_TEST_CASE;
}

int TestGetDocnumsNew() {
  START_TEST_CASE;
  int numdocs = 2000;
  int *docnums = NULL;
  docnums = (int *)malloc( sizeof(int) * numdocs );
  MALLOC_CHECK(docnums);
  BZERO(docnums, (sizeof(int) * numdocs));

  char *input = (char *)malloc(10);
  MALLOC_CHECK(input);
  BZERO(input, 10);
  strcat(input, "Welcome");

  getdocnumsnew(input, docnums, numdocs);

  //Since this directory is crawled from the home page, and the home page has "welcome" on it, this should be true
  SHOULD_BE( 0 != docnums[1] );

  free(docnums);
  free(input);

  END_TEST_CASE;
}

int TestGetDocnumsAppend() {
  START_TEST_CASE;
  int numdocs = 2000;
  int *docnums = NULL;
  docnums = (int *)malloc( sizeof(int) * numdocs );
  MALLOC_CHECK(docnums);
  BZERO(docnums, (sizeof(int) * numdocs));

  //First, we'll use getdocnumsnew in order to set up the docnums array
  getdocnumsnew("Welcome", docnums, numdocs);

  getdocnumsappend("dartmouth", docnums, numdocs);
  
  //Since this directory is crawled from the home page, and the home page has "welcome" and "dartmouth" on it, this should be true
  SHOULD_BE( 0 != docnums[1] );

  free(docnums);

  END_TEST_CASE;
}

int TestConsolidation(int *docnums, int *finaldocnums, int numdocs) {
  for ( int i=0; i < numdocs; i++ ) {
    if ( ( 0 != docnums[i] ) && ( 0 == finaldocnums[i] ) ) {
      return 0;
    }
  }
  return 1;

}
int TestConsolidateDocNums() {
  START_TEST_CASE;
  int numdocs = 2000;
  //First, make two docnums structures to consolidate in a bit
  int *docnums = NULL;
  docnums = (int *)malloc( sizeof(int) * numdocs );
  MALLOC_CHECK(docnums);
  BZERO(docnums, (sizeof(int) * numdocs));

  int *finaldocnums = NULL;
  finaldocnums = (int *)malloc( sizeof(int) * numdocs );
  MALLOC_CHECK(docnums);
  BZERO(finaldocnums, (sizeof(int) * numdocs));

  //Next, we'll use getdocnumsnew in order to set up the docnums array for the word "Welcome"
  getdocnumsnew("Welcome", docnums, numdocs);

  //Next, we'll set up finaldocnums for the word "dartmouth"
  getdocnumsnew("dartmouth", finaldocnums, numdocs);

  //Now, consolidate the two into finaldocnums
  consolidatedocnums(docnums, finaldocnums, numdocs);

  //The condition here is that, whenever the entry in docnums isn't zero, then finaldocnums should have a number greater than zero entered there
  SHOULD_BE(TestConsolidation(docnums, finaldocnums, numdocs));

  free(docnums);
  free(finaldocnums);

  END_TEST_CASE;
}


int TestRankeddoc(int *rankeddocs, int *docnums) {
  for( int i=0; rankeddocs[i+1]; i++ ) {
     if ( docnums[rankeddocs[i]] < docnums[rankeddocs[i+1]] ) {
	return 0;
     }
  }
  return 1;
}

int TestRankdocs() {
  START_TEST_CASE;
  int numdocs = 2000;
  int *docnums = NULL;
  docnums = (int *)malloc( sizeof(int) * numdocs );
  MALLOC_CHECK(docnums);
  BZERO(docnums, (sizeof(int) * numdocs));

  //First, we'll use getdocnumsnew in order to set up the docnums array
  getdocnumsnew("Welcome", docnums, numdocs);


  int *rankeddocs = NULL;

  rankeddocs = rankdocs(docnums, numdocs);

  //This makes sure that the documents are actually ranked correctly
  SHOULD_BE(TestRankeddoc(rankeddocs, docnums));

  free(docnums);
  free(rankeddocs);

  END_TEST_CASE;
}


int main( int argc, char** argv ) {
  int cnt = 0;

  initLists("index.dat");

  RUN_TEST(TestGetUrl, "TestGetUrl");
  RUN_TEST(TestGetAndCleanWordlist, "TestGetAndCleanWordlist");
  RUN_TEST(TestGetDocnumsNew, "TestGetDocnumsNew");
  RUN_TEST(TestGetDocnumsAppend, "TestGetDocnumsAppend");
  RUN_TEST(TestConsolidateDocNums, "TestConsolidateDocNums");
  RUN_TEST(TestRankdocs, "TestRankdocs");

  cleanupindexer(indexer);

  if (!cnt) {
    printf("All passed!\n");
    return 0;
  }
  else {
    printf("Some fails!\n");
    return 1;
  }
}


/*
  Method to clean up the wordlist
  @param wordlist - wordlist containing the words that must be freed, followed by the wordlist itself
 */
void cleanupwordlist(char **wordlist) {
  //First free the words, then wordlist
  for( int x = 0; (x < wordlistsize) && wordlist[x] && (0 != strcmp(wordlist[x], "\0")); x++ ) {
    free(wordlist[x]);
    wordlist[x] = NULL;
  }
  free(wordlist);
  wordlist = NULL;
}

/*
  Method to print the stdout the results of the query
  @param rankeddocs - an integer array of the ranked documents, where as the index increases, the ranking of the document falls
  @param dir - the directory to go to in order to find the names corresponding to each document ID
 */
void showqueryresults(int *rankeddocs, char *dir) {
  //For each document in rankeddocs, find its url in the target directory, and print out the information about the page
  for( int i = 0; rankeddocs[i] != 0; i++){
    char *url = geturl(dir, rankeddocs[i]);
      
    printf("Document ID:%d URL:%s", rankeddocs[i], url);

    free(url);
  }
}

/*
  Method to get the url from a document ID, given the target directory to look in
  @param directory - the directory containing the documents with names corresponding to the ID, and first lines as the url
  @param docnum - the document ID
 */
char *geturl(char *directory, int docnum) {
  //Make docname have space for 9 character + \0, which is more than it likely needs since the highest filename is likely in the thousands 
  //docname just contains a string representing the docnum
  char docname[10];
  sprintf(docname, "%d", docnum);

  //pathname stores the path to the file to be read
  char *pathname = (char *)malloc( strlen(directory) + 10 );
  MALLOC_CHECK(pathname);
  BZERO(pathname, (strlen(directory) + 10));

  strcat(pathname, directory);
  strcat(pathname, docname);
  
  //Open the file. If it doesn't open, return NULL
  FILE *fp = fopen(pathname, "r");
  if ( NULL == fp ) {
    return NULL;
  }
  
  //Allocate memory for a string, url, to store the url in. Read the first line of fp and put it into url
  char *url = (char *)malloc(MAX_URL_LENGTH);
  MALLOC_CHECK(url);
  BZERO(url, MAX_URL_LENGTH);
  
  fgets(url, MAX_URL_LENGTH, fp);


  //Free memory
  free(pathname);
  fclose(fp);

  return url;
}


/*
  Method to rank the documents in docnums and put them into a new integer array, rankeddocs
  @param docnums - the array of integers where the index refers to the document ID and the value refers to the value of that document
  @param numdocs - the number of documents in docnums
 */
int *rankdocs(int *docnums, int numdocs) {
  //Allocate memory for rankeddocs
  int *rankeddocs = (int *)malloc(sizeof(int) * numdocs);
  MALLOC_CHECK(rankeddocs);
  BZERO(rankeddocs, (sizeof(int) * numdocs));

  int numentries = numdocs;
  int highestfreq = 0; //Initialize to 0 so that any document's value > 0 exceeds it
  int highestdoc = 1; //Start with the highestdoc being 1

  //Have a nested for loop, where you go through the entries and find the highest value for a document. The outside loop is used for putting numbers into 
  //rankeddoc. The inner loop allows us to go through all the entries in numdocs every time
  for( int i = 0; i < numentries; i++ ){
    for( int j = 0; j < numentries; j++ ) {
      if( highestfreq < docnums[j] ) {
	highestdoc = j;
	rankeddocs[i] = highestdoc;
	highestfreq = docnums[j];
      }
    }
    
    //Once we've put the highestdoc of this iteration into rankeddocs, set the frequency of that document in docnums to -1 so that it's not considered the next time through
    //Then reset highestdoc and highestfreq
    docnums[highestdoc] = -1;
    highestdoc = 1;
    highestfreq = 0;
  }

  return rankeddocs;

}

/*
  Method that, when giving two integer arrays where the index refers to the document ID and the value refers to that document's value, will consolidate the frequencies into one docnum array, the final one. 
  @param docnums - the first integer array, not the one that will be used after this function is called
  @param finaldocnums - the second integer array, which will be used to look at the consolidated frequencies after this function is called
  @param numdocs - the number of documents to look at
 */
void consolidatedocnums(int *docnums, int *finaldocnums, int numdocs) {
  for( int i = 0; i < numdocs; i++ ) {
    finaldocnums[i] = finaldocnums[i] + docnums[i];
  }
}


/*
  Method to fill into docnums the frequencies of a word on all the webpages, up to numdocs. This method uses the indexer to find the pages with the word
  @param word - word to look for
  @param docnums - the integer array that's index refers to the document ID and value refers to that document's value
  @param numdocs - the maximum number of documents
 */
void getdocnumsnew(char *word, int *docnums, int numdocs) {
  DOCUMENTNODE *current_dnode;
  WORDNODE *current_wnode = indexer->start;

  //Loop through the indexer strcuture. If you find a wnode whose word is the same as the parameter word, then add that document into docnums
  while ( current_wnode != indexer->end ) {
    if ( 0 == strcasecmp(word, current_wnode->word) ) {
      current_dnode = current_wnode->page;

      while ( current_dnode ) {
	docnums[current_dnode->document_id] = current_dnode->page_word_frequency;
	current_dnode = current_dnode->next;
      }
    }

    current_wnode = current_wnode->next;
  }

  //Need to run this once more for the last node
   if ( 0 == strcasecmp(word, current_wnode->word) ) {
      current_dnode = current_wnode->page;

      while ( current_dnode ) {
	docnums[current_dnode->document_id] = current_dnode->page_word_frequency;
	current_dnode = current_dnode->next;
      }
    }
}


/*
  Method that, given an already inputted into docnums, will append the frequencies of a word on the webpages its found on, only if docnums has entries for a document already
  @param word - word to look for
  @param docnums - the integer array that's index refers to the document ID and value refers to that document's value
  @param numdocs - the maximum number of documents
 */
void getdocnumsappend(char *word, int *docnums, int numdocs) {
  //Make a temporary docnums which we'll use to find all the pages that this word is on
  int *tempdocnums = (int *)malloc(numdocs * sizeof(int));
  MALLOC_CHECK(tempdocnums);
  BZERO(tempdocnums, (numdocs * sizeof(int)));

  getdocnumsnew(word, tempdocnums, numdocs);


  //Then loop through both of the numdocs structures, and only if both document's entries at an index are not zero will we add the entries into docnums. 
  //If both documents don't contain the word, then even if docnums had it, set that entry in it to zero, since the condition of AND is that both docs have
  //the word
  for( int i=0; i < numdocs; i++ ) {
    if( ( 0 != docnums[i] ) && ( 0 != tempdocnums[i] ) ) {
      docnums[i] = docnums[i] + tempdocnums[i];
    }
    else {
      docnums[i] = 0;
    }
  }

  //Free the memory malloc'ed in this function
  free(tempdocnums);
}


/*
  Method to get the list of words from the user's query
  @param input - the string containing the user's query
  @param wordlist - the array of char * that will contain all the words from the query
 */
void getwordlist(char *input, char **wordlist) {
  char *token;
  char *delim = " \n";

  //numwords will let us know if the number of words they input is bigger than the pre-decided size of 100. If it is, then the words after it are ignored
  int numwords = 0;
 
  //Keep getting the words from the input and storing them into wordlist, until token is NULL
  token = strtok(input, delim);

  while ( ( NULL != token ) && ( numwords < wordlistsize ) ) {
    char *word = malloc(strlen(token)+5);
    MALLOC_CHECK(word);
    BZERO(word, (strlen(token)+5));
    strcat(word, token);
    wordlist[numwords] = word;

    numwords++;

    token = strtok(NULL, delim);
  }
}


/*
  Method to initialize the INVERTED_INDEX * data structure
  @param myindex - index to be initialized
  @return 1 upon success
*/
int initLists(char *indexfile) {
  indexer = readFile("../crawler/data2/", indexfile);

  if ( NULL != indexer ) {
    return 1;
  }
  else {
    return 0;
  }
}
