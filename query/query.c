/*

  FILE: query.c

  Description: A function to recreate an inverted_index from the file outputted by indexer. This function will take user inputs and output to stdout the web pages, in descending order, that show their query most often

  Inputs: ./query [ FILENAME ] [ TARGET DIRECTORY ]

  Outputs: Continually responds to the user's query with the webpages that display their results

  Implementation Spec:
    While (User's query isn't ^C)
	1: Get the user's query
	2: Parse out individual words within the query
	3: Get the values for each of the documents in the target directory based off the query
	4: Rank the documents in the target directory using their values
	5: In descending order based off rank, read the url from the document and print out the info to the user
    After ^C command:
	Cleanup all data structures
	Exit
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "header.h"
#include "../indexer/indexer.h"
#include "../util/file.h"
#include "../util/hash.h"
#include "../util/input.h"
#include "../util/indexerutils.h"
#include "query.h"

INVERTED_INDEX *indexer = NULL;

int execute = 1; //Keeps us in the while loop until Ctrl+C is pressed
int wordlistsize = 100;

int main( int argc, char *argv[] ) {
  //Check parameters
  //Check the number of inputs
  checknuminputsone((argc-1), 2);

  //Check that the inputs are of the correct type
  checkInputs(argv[1], argv[2]);

  //If we get here, store the inputs in variables
  char *indexfile = argv[1];
  char *dir = argv[2];


  //Initialize data structures. Quit if the initialization goes wrong
  if ( initLists(indexfile) != 1) {
    fprintf(stderr, "-------------------------\n");
    fprintf(stderr, "Initialization gone wrong for indexer. Exiting.\n");
    fprintf(stderr, "-------------------------\n");
    exit(1);
  }

  //Set up a signal trapping for the SIGINT signal
  signal(SIGINT, &trap);


  //Set up some of the variables that are used below in the infinite loop
  int numbytes = 512; //Number of bytes the program will read from the query

  char buffer[numbytes]; //Buffer for the user input

  char *input = (char *)malloc(numbytes); //String that eventually holds the buffer for later functions
  MALLOC_CHECK(input);

  char **wordlist; //Contains the words in the user's query
  int *docnums; //Array of integers, where the indices 
                //refer to the document ID and the values refer to the frequency of the words
  int *olddocnums; //Same as docnums, but used to keep 
                   //track of part of query before "OR" if the "OR" statement is used
  char *url; //url corresponding to the document ID
  int *rankeddocs; //Integer array where, as the increase indices 
                   //as the documents' ranks decrease. So the document with the highest rank is at index 0
  
  while( execute ) {
    //Initialize everything to NULL at the beginning of every loop
    wordlist = NULL;
    docnums = NULL;
    olddocnums = NULL;
    url = NULL;
    rankeddocs = NULL;
    BZERO(buffer, numbytes);
    BZERO(input, numbytes);

    printf("KEY WORD:>");

    //Get the input. Once you get it, check if we should still 
    //execute based off the input. If we're told to quit, break
    fgets(buffer, numbytes, stdin);
    if( !execute ) {
      break;
    }

    strcat(input, buffer);

    printf("[query.c]Query...\n"); //Talk to user

    //Copy the words from the input into the wordlist
    wordlist = (char **)malloc( sizeof(char *) * (wordlistsize+1));
    MALLOC_CHECK(wordlist);
    BZERO(wordlist, (sizeof(char *) * (wordlistsize+1)));
    getwordlist(input, wordlist);

    //I assume that the greatest number of document we'll see is 2000
    int numdocs = 2000;

    //Initialize docnums
    docnums = (int *)malloc( sizeof(int) * numdocs );
    MALLOC_CHECK(docnums);
    BZERO(docnums, (sizeof(int) * numdocs));

    olddocnums = NULL;

    //firstcall is used in the loop below, since the first word in the query has to be treated differently.
    //firstcall corresponds to either the first word in the query, 
    //or the first word after an OR statement, since that has to be treated differently too
    int firstcall = 1;

    for( int i = 0; NULL != wordlist[i] ; i++ ) {
      //If the word is AND, just go to the next word
      if ( 0 == strcmp(wordlist[i], "AND") ) {
      }

      //If the word is neither AND nor OR, go here
      else if ( 0 != strcmp(wordlist[i], "OR") ) {
	//If it's the firstcall, then start docnums from scratch with this word, and set firstcall to zero
	if ( firstcall ) {
	  getdocnumsnew(wordlist[i], docnums, numdocs);
	  firstcall = 0;
	}
	//Otherwise, only append to docnums the documents that already have a frequency
	else {
	  getdocnumsappend(wordlist[i], docnums, numdocs);
	}
      }

      //If the input was OR, one of two things happens. 
      //If this was the first OR, then olddocnums is NULL, so just set it equal to docnums and malloc
      //new memory for docnums. Otherwise, we have to consolidate the olddocnums and docnums, store
      //that consolidation in olddocnums, then malloc new memory for docnums
      else {
	if ( NULL == olddocnums ) {
	  olddocnums = docnums;
	}
	else {
	  consolidatedocnums(docnums, olddocnums, numdocs);
	  free(docnums);
	}

	docnums = (int *)malloc( sizeof(int) * numdocs );
	MALLOC_CHECK(docnums);
	BZERO(docnums, (sizeof(int) * numdocs));
	
	firstcall = 1;
      }
    }
    //At the end of the loop, free olddocnums after consolidating its information into docnums
    if ( NULL != olddocnums ) {
      consolidatedocnums(olddocnums, docnums, numdocs);
      free(olddocnums);
    }

    //Put the information relevant to the results' rankings into rankeddocs
    rankeddocs = rankdocs(docnums, numdocs);

    printf("[query.c]Done\n\n"); //Let the user know you're done

    showqueryresults(rankeddocs, dir); //Then print the results

    //Clean up some memory
    free(rankeddocs);
    free(docnums);
    cleanupwordlist(wordlist);
  }

  //The signal for ending the program is caught here, which allows us 
  //to free up the memory structures, provided that they're not NULL, which
  //would be the case if the SIGINT signal was sent on the first call
  signal(SIGINT, SIG_DFL);
  
  if( NULL != wordlist ) {
    cleanupwordlist(wordlist);
  }
  if( NULL != docnums ) {
    free(docnums);
  }
  if( NULL != olddocnums ) {
    free(docnums);
  }
  if( NULL != url ) {
    free(url);
  }
  if( NULL != rankeddocs ){
    free(rankeddocs);
  }

  //We'll always need to free input and indexer
  free(input);
  cleanupindexer(indexer);
}

/*
  Method to clean up the wordlist
  @param wordlist - wordlist containing the words that must be freed, followed by the wordlist itself
 */
void cleanupwordlist(char **wordlist) {
  //First free the words, then wordlist
  for( int x = 0; (x < wordlistsize) && wordlist[x] && (0 != strcmp(wordlist[x], "\0")); x++ ) {
    free(wordlist[x]);
    wordlist[x] = NULL;
  }
  free(wordlist);
  wordlist = NULL;
}

/*
  Method to print the stdout the results of the query
  @param rankeddocs - an integer array of the ranked documents, where as the index increases, the ranking of the document falls
  @param dir - the directory to go to in order to find the names corresponding to each document ID
 */
void showqueryresults(int *rankeddocs, char *dir) {
  //For each document in rankeddocs, find its url in the target directory, and print out the information about the page
  for( int i = 0; rankeddocs[i] != 0; i++){
    char *url = geturl(dir, rankeddocs[i]);
      
    printf("Document ID:%d URL:%s", rankeddocs[i], url);

    free(url);
  }
}

/*
  This method allows us to capture the SIGINT signal. It sets execute to 0, which allows us to break out of the while loop
  @param signal - a number corresponding to the SIGINT symbol
 */
void trap(int signal) {
  execute = 0;
}

/*
  Method to get the url from a document ID, given the target directory to look in
  @param directory - the directory containing the documents with names corresponding to the ID, and first lines as the url
  @param docnum - the document ID
 */
char *geturl(char *directory, int docnum) {
  //Make docname have space for 9 character + \0, which is more than it 
  //likely needs since the highest filename is likely in the thousands 
  //docname just contains a string representing the docnum
  char docname[10];
  sprintf(docname, "%d", docnum);

  //pathname stores the path to the file to be read
  char *pathname = (char *)malloc( strlen(directory) + 10 );
  MALLOC_CHECK(pathname);
  BZERO(pathname, (strlen(directory) + 10));

  strcat(pathname, directory);
  strcat(pathname, docname);
  
  //Open the file. If it doesn't open, return NULL
  FILE *fp = fopen(pathname, "r");
  if ( NULL == fp ) {
    return NULL;
  }
  
  //Allocate memory for a string, url, to store the url in. Read the first line of fp and put it into url
  char *url = (char *)malloc(MAX_URL_LENGTH);
  MALLOC_CHECK(url);
  BZERO(url, MAX_URL_LENGTH);
  
  fgets(url, MAX_URL_LENGTH, fp);


  //Free memory
  free(pathname);
  fclose(fp);

  return url;
}


/*
  Method to rank the documents in docnums and put them into a new integer array, rankeddocs
  @param docnums - the array of integers where the index refers to the document ID and the value refers to the value of that document
  @param numdocs - the number of documents in docnums
 */
int *rankdocs(int *docnums, int numdocs) {
  //Allocate memory for rankeddocs
  int *rankeddocs = (int *)malloc(sizeof(int) * numdocs);
  MALLOC_CHECK(rankeddocs);
  BZERO(rankeddocs, (sizeof(int) * numdocs));

  int numentries = numdocs;
  int highestfreq = 0; //Initialize to 0 so that any document's value > 0 exceeds it
  int highestdoc = 1; //Start with the highestdoc being 1

  //Have a nested for loop, where you go through the entries and find the highest value for a document.
  //The outside loop is used for putting numbers into 
  //rankeddoc. The inner loop allows us to go through all the entries in numdocs every time
  for( int i = 0; i < numentries; i++ ){
    for( int j = 0; j < numentries; j++ ) {
      if( highestfreq < docnums[j] ) {
	highestdoc = j;
	rankeddocs[i] = highestdoc;
	highestfreq = docnums[j];
      }
    }
    
    //Once we've put the highestdoc of this iteration into rankeddocs, set the
    //frequency of that document in docnums to -1 so that it's not considered the next time through
    //Then reset highestdoc and highestfreq
    docnums[highestdoc] = -1;
    highestdoc = 1;
    highestfreq = 0;
  }

  return rankeddocs;

}

/*
  Method that, when giving two integer arrays where the index refers to the document ID and the value refers to that document's value, will consolidate the frequencies into one docnum array, the final one. 
  @param docnums - the first integer array, not the one that will be used after this function is called
  @param finaldocnums - the second integer array, which will be used to look at the consolidated frequencies after this function is called
  @param numdocs - the number of documents to look at
 */
void consolidatedocnums(int *docnums, int *finaldocnums, int numdocs) {
  for( int i = 0; i < numdocs; i++ ) {
    finaldocnums[i] = finaldocnums[i] + docnums[i];
  }
}


/*
  Method to fill into docnums the frequencies of a word on all the webpages, up to numdocs. This method uses the indexer to find the pages with the word
  @param word - word to look for
  @param docnums - the integer array that's index refers to the document ID and value refers to that document's value
  @param numdocs - the maximum number of documents
 */
void getdocnumsnew(char *word, int *docnums, int numdocs) {
  DOCUMENTNODE *current_dnode;
  WORDNODE *current_wnode = indexer->start;

  //Loop through the indexer strcuture. If you find a wnode whose word is the same as the parameter word,
  //then add that document into docnums
  while ( current_wnode != indexer->end ) {
    if ( 0 == strcasecmp(word, current_wnode->word) ) {
      current_dnode = current_wnode->page;

      while ( current_dnode ) {
	docnums[current_dnode->document_id] = current_dnode->page_word_frequency;
	current_dnode = current_dnode->next;
      }
    }

    current_wnode = current_wnode->next;
  }

  //Need to run this once more for the last node
   if ( 0 == strcasecmp(word, current_wnode->word) ) {
      current_dnode = current_wnode->page;

      while ( current_dnode ) {
	docnums[current_dnode->document_id] = current_dnode->page_word_frequency;
	current_dnode = current_dnode->next;
      }
    }
}


/*
  Method that, given an already inputted into docnums, will append the frequencies of a word on the webpages its found on, only if docnums has entries for a document already
  @param word - word to look for
  @param docnums - the integer array that's index refers to the document ID and value refers to that document's value
  @param numdocs - the maximum number of documents
 */
void getdocnumsappend(char *word, int *docnums, int numdocs) {
  //Make a temporary docnums which we'll use to find all the pages that this word is on
  int *tempdocnums = (int *)malloc(numdocs * sizeof(int));
  MALLOC_CHECK(tempdocnums);
  BZERO(tempdocnums, (numdocs * sizeof(int)));

  getdocnumsnew(word, tempdocnums, numdocs);


  //Then loop through both of the numdocs structures and only if both document's 
  //entries at an index are not zero will we add the entries into docnums. 
  //If both documents don't contain the word, then even if docnums had it, set that entry in it to zero, since the condition of AND is that both docs have
  //the word
  for( int i=0; i < numdocs; i++ ) {
    if( ( 0 != docnums[i] ) && ( 0 != tempdocnums[i] ) ) {
      docnums[i] = docnums[i] + tempdocnums[i];
    }
    else {
      docnums[i] = 0;
    }
  }

  //Free the memory malloc'ed in this function
  free(tempdocnums);
}


/*
  Method to get the list of words from the user's query
  @param input - the string containing the user's query
  @param wordlist - the array of char * that will contain all the words from the query
 */
void getwordlist(char *input, char **wordlist) {
  char *token;
  char *delim = " \n";

  //numwords will let us know if the number of words they input is bigger than the pre-decided size of 100. If it is, then the words after it are ignored
  int numwords = 0;
 
  //Keep getting the words from the input and storing them into wordlist, until token is NULL
  token = strtok(input, delim);

  while ( ( NULL != token ) && ( numwords < wordlistsize ) ) {
    char *word = malloc(strlen(token)+5);
    MALLOC_CHECK(word);
    BZERO(word, (strlen(token)+5));
    strcat(word, token);
    wordlist[numwords] = word;

    numwords++;

    token = strtok(NULL, delim);
  }
}

/*
  Method that is used to check that the user's inputs are ok
  @param indexfile - the file that contains the indexer
  @param dir - the target directory
 */
void checkInputs(char *indexfile, char *dir) {
  testDir(dir);

  if ( 0 == testfile(indexfile) ) {
    fprintf(stderr, "------------------------------\n");
    fprintf(stderr, "Invalid index file. Exiting...\n");
    fprintf(stderr, "------------------------------\n");
    exit(1);
  }
}


/*
  Method to initialize the INVERTED_INDEX * data structure
  @param myindex - index to be initialized
  @return 1 upon success
*/
int initLists(char *indexfile) {
  indexer = readFile("", indexfile);

  if ( NULL != indexer ) {
    return 1;
  }
  else {
    return 0;
  }
}
