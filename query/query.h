#ifndef _QUERY_H
#define _QUERY_H

//This file just contains the list of functions that are in query.c. Besides this, all the functionality needed
//can be found elsewhere in the other included files.

//Functions that are used in query.c are declared here

// Displays the results of the query to the user through print statements
void showqueryresults(int *rankeddocs, char *dir);


// Given a directory and a document number, get the url written on the first line of that
// document and return it
char *geturl(char *directory, int docnum);


// Given the query, and an empty array of strings, separate all the words and store them
// individually in wordlist
void getwordlist(char *input, char **wordlist);


// Initialize the indexer data structure by calling readFile (from file.c).
int initLists(char *indexfile);


// Method to call some of the functions from input.c in the utils directory. It makes sure
// the inputs are proper.
void checkInputs(char *indexfile, char *dir);


// Method to rank the documents based off their value relative to the frequency of the words
// from the query
int *rankdocs(int *docnums, int numdocs);


// Method to do the OR operation on two int arrays that store the values of documents relative
// to two queries
void consolidatedocnums(int *docnums, int *finaldocnums, int numdocs);


// Method to get the document values for a word in a query. This function is used when we aren't
// in the AND operation, since it just takes the frequencies of the word and puts them directly
// into docnums
void getdocnumsnew(char *word, int *docnums, int numdocs);


// This method is like getdocnumsnew, except it is used when we're in the AND operation, since
// then you only want to add document frequencies if the document already has a value
void getdocnumsappend(char *word, int *docnums, int numdocs);


// This method is used to allow us to exit the program when the user's query is Ctrl+C
void trap(int signal);


// This method cleans up the char **wordlist structure
void cleanupwordlist(char **wordlist);

#endif
