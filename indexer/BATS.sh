#!/bin/bash
# Script name: BATS.sh
#
# Description: A script to run tests of the indexer program for Lab 5
#
# Input: none
#
# Output: a file called indexer_testlog.`date`, with the bash output from the date all there
#

date=`date`
filename="indexer_testlog.$date"

touch "$filename"

printf "Date at beginning of build $date\n" > "$filename"
printf "Tests run on kinsman host on the Linux operating system\n" >> "$filename"
make clean &>> "$filename"
make indexer &>> "$filename"
printf "Date at end of build `date`\n" >> "$filename"

#First do the tests that show its ability to capture errors
printf "\nTests to demonstrate the ability of indexer to capture errors\n" &>> "$filename"
printf "Assumptions of this test file are that directory ../crawler/data2/ is accessible, that an accessible directory ./test/ is empty, that an accessible directory ./test2/ has only another directory inside it, ./test2/interior/, that an accessible directory called ./nowritedir/ is non-writable, and that there is no directory named ./fakedir/.\n" &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer\n" &>> "$filename"
printf "Expected output informs the user that they had an improper number of inputs and exits the program\n" &>> "$filename"
./indexer &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ../crawler/data2/ indexer\n" &>> "$filename"
printf "Expected output informs the user of an invalid filename, since it's named indexer, and exits the program.\n" &>> "$filename"
./indexer ../crawler/data2/ indexer &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ./test/ index.dat\n" &>> "$filename"
printf "Expected output informs the user of an invalid target directory as it's empty, and exits the program.\n" &>> "$filename"
./indexer ./test/ index.dat &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ./nowritedir/ index.dat \n" &>> "$filename"
printf "Expected output informs the user of an invalid target directory as it's nonwritable, and exits the program.\n" &>> "$filename"
./indexer ./nowritedir/ index.dat &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ./test2/ index.dat\n" &>> "$filename"
printf "Expected output informs the user of an invalid directory, as it's empty (the directory inside it doesn't count).\n" &>> "$filename"
./indexer ./test2/ index.dat &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ./fakedir/ index.dat\n" &>> "$filename"
printf "Expected output informs the user of an invalid directory, as it's non-existent.\n" &>> "$filename"
./indexer ./fakedir/ index.dat &>> "$filename"

printf "\n\nNow I'll run tests that are expected to work\n" &>> "$filename"
printf "\nNew test\n" &>> "$filename"
printf "./indexer ../crawler/data2/ index.dat\n" &>> "$filename"
printf "Expected output will be a file named index.dat, which will be located in the folder ../crawler/data2/\n" &>> "$filename"
./indexer ../crawler/data2/ index.dat &>> "$filename"

printf "\nNew test\n" &>> "$filename"
printf "./indexer ../crawler/data2/ index1.dat index1.dat new_index1.dat\n" &>> "$filename"
printf "Expected output will be two files in ../crawler/data2/. The first is index1.dat, the second is new_index1.dat. They should both represent the same inverted index. Along the way, indexer will print a message that it skipped over index.dat when reading files, since that file was produced by the previous test in the target directory. This shows part of the program's ability to handle errors as well.\n" &>> "$filename"
./indexer ../crawler/data2/ index1.dat index1.dat new_index1.dat &>> "$filename"

printf "\nNow I'll show that the indexer did in fact produce two files that are the same by the following command:\ndiff ../crawler/data2/index1.dat ../crawler/data2/new_index1.dat\nIf this works, nothing will appear on the following line.\n" &>> "$filename"
diff ../crawler/data2/index1.dat ../crawler/data2/new_index1.dat &>> "$filename"
