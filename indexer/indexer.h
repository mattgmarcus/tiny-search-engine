#ifndef _INDEXER_H_
#define _INDEXER_H_

//Defining constants here
#define MAX_URL_LENGTH 2049

#define MAX_HASH_SLOT 10000

#define MAX_NUMBER_SLOTS 10000

#define WORD_LENGTH 5000 //5000 is more than enough in this case, but I have that many to be safe


//The DOCUMENTNODE struct is used in a linked list structure of DOCUMENTNODES. Each node keeps track of the id of the document it corresponds to, as well as the number of times it has the word it corresponds to. It also has a pointer to the next item in the linked list. 
//These nodes are stored by WORDNODES. Each linked list of DOCUMENTNODES only corresponds to one word , but any number of documents that contain it
typedef struct _DocumentNode {
  struct _DocumentNode *next;        // pointer to the next member of the list.
  int document_id;                   // document identifier
  int page_word_frequency;           // number of occurrences of the word
} 
	DOCUMENTNODE;

//The WORDNODE structure is used for storing information about all the words indexer comes across. Each node has a word that it stores, as well as pointer to the next and previous word in the doubley-linked list of WORDNODES. Additionally, it stores a pointer to a singley-linked list of DOCUMENTNODES, which keeps track of which documents have the word. Finally, it has an int numdocs, that keeps track of how many documents had this word in it
typedef struct _WordNode {
  struct _WordNode *prev;           // pointer to the previous word
  struct _WordNode *next;           // pointer to the next word
  char word[WORD_LENGTH];           // the word
  DOCUMENTNODE  *page;              // pointer to the first element of the page list.
  int numdocs;                      //number of documents that have this word
}
 WORDNODE;


//The INVERTED_INDEX is the overarching structure that keeps track of all the WORDNOES. It has a hash table that you can you to quickly and easily access WORDNODES. It also keeps track of the start and end of the doubley-linked list of WORDNODES
typedef struct _INVERTED_INDEX {
                                        // Start and end pointer of the dynamic links.
  WORDNODE *start;                      // start of the list
  WORDNODE *end;                        // end of the list
  WORDNODE *hash[MAX_NUMBER_SLOTS];  // hash slot
} 
 INVERTED_INDEX;

//I had to name this indexer, instead of index, because a variable name index is defined in string.h
INVERTED_INDEX *indexer;


//Here are some of the methods that are used in indexer.c
char *loadDocument(char* fileName, char *dir);

int getDocumentID(char* fileName);

//int updateIndex(char* word, int documentID, INVERTED_INDEX *myindex);

//int saveIndexToFile(char *dir, char *filename, INVERTED_INDEX *myindex);

INVERTED_INDEX* readfile(char* file);

void buildIndexFromDirectory(char *path);

//void iaddexisting(char *word, int documentID, WORDNODE *current_wnode);
//WORDNODE *iaddnew(char *word, int documentID);

//void cleanupindexer(INVERTED_INDEX *myindex);


#endif
