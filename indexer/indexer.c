/*

  FILE: indexer.c

  Description: A function to create an inverted_index from the files outputted by crawler. This function will output the contents of the index to a file that can be easily re-read to re-create the index

  Inputs: ./indexer [TARGET DIRECTORY] [RESULTS FILENAME]
       OR ./indexer [TARGET DIRECTORY] [RESULTS FILENAME] [RESULTS FILENAME] [REWRITTEN FILENAME]

  Outputs: If you do the first input case, then a file will be ouputted with the name given by [RESULTS FILENAME]. This file will contain the contents of the index.
  If you do the second input case, then you will have the same output as the first case, but with an additional file named [REWRITTEN FILENAME]. These 2 files should contain the same information
*/

#define _GNU_SOURCE
#include <stdlib.h>
#include <stdio.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>
#include "indexer.h"
#include "../util/hash.h"
//#include "hash.h"
//#include "../util/hash.c"
#include "header.h"
#include "../util/file.h"
//#include "../util/file.c"
#include "../util/input.h"
//#include "../util/input.c"
#include "../util/indexerutils.h"

INVERTED_INDEX* indexer = NULL;

int initLists();
void cleanup(char *dir);
void cleanup3();
void addWords(char *loadedDoc, int docID);

int main( int argc, char *argv[] ) {
  //Create some instance variables here. dir and resultsfile are always used. resultsfile2 and rewritefile may be used if there are 5 inputs
  char *dir;
  char *resultsfile;
  char *resultsfile2;
  char *rewritefile;
  //Test command-line options
  //First, test number of inputs
  checknuminputstwo((argc-1), 2, 4);
  
  //Check the directory
  testDir(argv[1]);

  //We'll always have to check the filename for the second input
  if ( 1 == testfile(argv[2]) ){
    fprintf(stderr, "----------------------------------------\n");
    fprintf(stderr, "Invalid filename. Cannot be %s. Exiting.\n", argv[2]);
    fprintf(stderr, "----------------------------------------\n");
    exit(1);
  }

  //Initialize *dir and *resultsfile
  dir = argv[1];
  resultsfile = argv[2];

  //If they do the long input option, check the next two filenames as well
  if ( 5 == argc ) {
    if ( 1 == testfile(argv[3]) ){
      fprintf(stderr, "----------------------------------------\n");
      fprintf(stderr, "Invalid filename. Cannot be %s. Exiting.\n", argv[3]);
      fprintf(stderr, "----------------------------------------\n");
      exit(1);
    }
    if ( 1 == testfile(argv[4]) ){
      fprintf(stderr, "----------------------------------------\n");
      fprintf(stderr, "Invalid filename. Cannot be %s. Exiting.\n", argv[4]);
      fprintf(stderr, "----------------------------------------\n");
      exit(1);
    }
    //Initialize variables for the last 2 inputs
    resultsfile2 = argv[3];
    rewritefile = argv[4];
  }
  //Initialize data structures. Quit if the initialization goes wrong
  if ( initLists() != 1) {
    fprintf(stderr, "-------------------------\n");
    fprintf(stderr, "Initialization gone wrong for indexer. Exiting.\n");
    fprintf(stderr, "-------------------------\n");
    exit(1);
  }

  //Build up indexer, then save it to resultsfile
  buildIndexFromDirectory(dir);

  saveIndexToFile(dir, resultsfile, indexer);

  //Cleanup indexer and directory structure here, and remove the temporary file created
  cleanup(dir);

  //If they inputted 5 arguments, recreate the index from the outputted file and then reoutput it to rewritefile
  if ( 5 == argc ) {
    INVERTED_INDEX* indexer2 = readFile(dir, resultsfile2);
    
    //Make sure that indexer2 isn't NULL. If it is, then there was an issue reading the resultsfile, so we should quit here
    if ( NULL == indexer2 ) {
      fprintf(stderr, "-------------------------------------------\n");
      fprintf(stderr, "Error reading %s and recreating %s. Exiting\n", resultsfile, resultsfile2);
      fprintf(stderr, "-------------------------------------------\n");
      exit(1);
    }
    saveIndexToFile(dir, rewritefile, indexer2);
    cleanupindexer(indexer2);
  }

  return 0;
}

/*
  Method to build indexer using all the files in the target directory
  @param path - the target directory
 */
void buildIndexFromDirectory(char *path) {
  char *doc; //stores text of document
  int docID = 0;
  char *file; //stores name of file
  int notemptydir = 0; //Lets us know if the target directory is empty

  //Go through all the files in path
  while ( NULL != (file = FindFilesInPath(path)) ) {
    struct stat statbuf;
    char *pathname = malloc( strlen(path) + strlen(file) + 5 );
    MALLOC_CHECK(pathname);
    BZERO(pathname, ( strlen(path) + strlen(file) + 5 ));
    strcat(pathname, path);
    strcat(pathname, file);
    stat(pathname, &statbuf);

    //Make sure to not read the filenames for the current and prior directories, or any directory
    if ( ( strcmp(file, ".") == 0 ) || ( strcmp(file, "..") == 0 ) || (S_ISDIR(statbuf.st_mode)) ) {
      free(pathname);
      continue;
    }
    free(pathname);

    doc = loadDocument(file, path);
    docID = getDocumentID(file);

    //If the docID is zero, then that wasn't a valid document, so free whatever information we got and continue
    if ( 0 == docID ) {
      free(doc);
      continue;
    }

    //Add the word we got to indexer
    addWords(doc, docID);

    free(doc);

    if ( 0 == notemptydir ) {
      notemptydir = 1;
    }
  }

  //This is only done if the user inputs an empty target directory, in which case doc will never have been made into anything. We just have to make sure to clean up the indexer
  if ( 0 == notemptydir ) {
    fprintf(stderr, "---------------------------------------------------------------------\n");
    fprintf(stderr, "You can't input an empty directory for the target directory. Exiting.\n");
    fprintf(stderr, "---------------------------------------------------------------------\n");
    cleanup3();
    exit(1);
  }
    
}



/*
  Method to initialize the INVERTED_INDEX * data structure
  @param myindex - index to be initialized
  @return 1 upon success
*/
int initLists() {
  //Initialize indexer
  indexer = (INVERTED_INDEX *)malloc( sizeof(INVERTED_INDEX) );
  MALLOC_CHECK(indexer);
  BZERO(indexer, sizeof(INVERTED_INDEX));

  //Initialize both the start and end of indexer to be null
  indexer->start = indexer->end = NULL;
  
  //Return 1 upon succcess
  return 1;
}


/*
  Method to load a file containing the text from a crawler output file
  @param fileName - name of file to be read
  @param dir - directory that fileName is in
  @return text from page
*/
char *loadDocument(char *fileName, char *dir) {
  //pathname has the location of fileName
  char *pathname = malloc(strlen(dir) + strlen(fileName) + 5);
  MALLOC_CHECK(pathname);
  BZERO(pathname, (strlen(dir) + strlen(fileName) + 5));
  strcat(pathname, dir);
  strcat(pathname, fileName);

  //temppath has the location we place the temp file into (it will be deleted leater in cleanup)
  char *temppath = malloc(strlen(dir) + 15);
  MALLOC_CHECK(temppath);
  BZERO(temppath, (strlen(dir) + 15));
  strcat(temppath, dir);
  strcat(temppath, "temp");

  //We use bashcall to take the file in target directory, get out all the html tags and ^M characters (using dos2unix), and store that in temp
  char *bashcall = malloc(100);
  MALLOC_CHECK(bashcall);
  BZERO(bashcall, 100);
  strcat(bashcall,"tail -n +3 "); //This will take out the line with the url and depth
  strcat(bashcall, pathname);
  strcat(bashcall, " | sed 's/<[^>]*>/ /g' | dos2unix > "); //remove html tags and ^M
  strcat(bashcall, temppath);
  system(bashcall);

  //Use stat to see how many bytes are in the page
  struct stat st;
  /*stat(pathname, &st);*/
  stat(temppath, &st);
  int numbytes = st.st_size;

  //filebuffer stores the information that is contained in the webpage
  char *filebuffer = (char *)malloc(numbytes + 1);
  MALLOC_CHECK(filebuffer);
  BZERO(filebuffer, (numbytes + 1));

  //Get the temp file open so we can read it and put it into filebuffer
  FILE *fp = fopen(temppath, "r");

  //Return NULL if the file is NULL
  if ( NULL == fp ) {
    return NULL;
  }
  
  //Read all the characters from file and store them in the filebuffer
  int c = getc(fp);
  for ( int x = 0; c != EOF; x++ ){
    filebuffer[x] = c;
    c = getc(fp);
  }
  //Add a NULL character to the end
  strcat(filebuffer, "\0");

  fclose(fp);
  free(pathname);
  free(temppath);
  free(bashcall);
  
  return filebuffer;
}

/*
  Method to clean up data structure after the first part of indexer (with 3 inputs) concludes
  @param dir - target directory, where the temp file is located in
*/
void cleanup(char *dir) {
  //Cleanup the indexer structure
  WORDNODE *current_wnode = indexer->start;
  while ( current_wnode != indexer->end ) {
    current_wnode = current_wnode->next;
    DOCUMENTNODE *current_dnode = current_wnode->prev->page;
    DOCUMENTNODE *temp_dnode;
    while ( NULL != current_dnode->next ) {
      temp_dnode = current_dnode->next;
      free(current_dnode);
      current_dnode = temp_dnode;
    }
    free(current_dnode);
    free(current_wnode->prev);
  }
  //That loop won't free the last element in indexer, so do that here
  free(indexer->end->page);
  free(indexer->end);

  //Free indexer itself
  free(indexer);

  //Clean up the dirent structure
  dircleanup();

  //Delete the temp file that was made
  char *bashcall = malloc(strlen(dir) + 15);
  MALLOC_CHECK(bashcall);
  BZERO(bashcall, (strlen(dir) + 15));
  strcat(bashcall, "rm ");
  strcat(bashcall, dir);
  strcat(bashcall, "temp");
  system(bashcall);
  free(bashcall);
}


/*
  Method to clean up data structures if the user inputs an empty target directory. That is the only time this function can be called.
*/
void cleanup3() {
  //Cleanup the indexer structure. There's less to do than normal since less was allocated before
  //Free indexer itself
  free(indexer);

  //Clean up the dirent structure
  dircleanup();
}

/*
  Method to get the document ID from a fileName. Returns 0 if there is an issue with the fileName, and reports to stderr
  @param fileName - name of file
  @return document ID upon successful conversion, 0 otherwise
*/
int getDocumentID(char* fileName) {
  int returnnum = atoi(fileName);

  //If the fileName converted to a number was zero, yet the filename consists of letters, then this is a bad fileName as it doesn't follow the naming conventions
  if ( ( 0 == returnnum ) && ( isalpha(fileName[0]) ) ) {
      fprintf(stderr, "-----------------------------------------------\n");
      fprintf(stderr, "Invalid filename: %s. Skipping to next document\n", fileName);
      fprintf(stderr, "-----------------------------------------------\n");
      return 0;
    }

  return returnnum;
}

/*
  Method to add the words and document ID from a document to indexer
  @param loadedDoc - buffer containing contents of the file
  @param docID - document ID number
*/
void addWords(char *loadedDoc, int docID) {
  //token will store the word we get out of the buffer
  char *token;
  char *delim = " \n\t\r.:,\"()!?;&#=<>~";
  
  int successadd; //let's us know if we've succesfully put a word into indexer

  token = strtok(loadedDoc, delim);
  //While we're getting words, do add them to indexer
  while ( token ) {
    //Only allow words that are 3 or more characters
    if ( 3 <= strlen(token) ) { 
      successadd = updateIndex(token, docID, indexer);
    
      if ( 0 == successadd ) {
	fprintf(stderr, "-------------------------------\n");
	fprintf(stderr, "Error adding word %s to indexer\n", token);
	fprintf(stderr, "-------------------------------\n");
      }
    }
   token = strtok(NULL, delim);
  }
}

